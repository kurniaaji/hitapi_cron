<?php

// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', '1');
date_default_timezone_set("Asia/Jakarta");

Class DbOperation {

	private $database;
    
    /**
    * Constructor to allocate an instance and initialize its private variables.
    */
    public function __construct() {
    	require_once 'koneksi.php';

        $database = new Connection();
		$this->database = $database;
    }

    public function saveDataproductlog($data = array()) {

    	try {
		    $db = $this->database->openConnection();
		 	
		    // inserting data into create table using prepare statement to prevent from sql injections
		    $query = $db->prepare("INSERT INTO product_logs(idpel,request,response,request_date,response_date,status,product_id) VALUES (:idpel,:request,:response,:request_date,:response_date,:status,:product_id)");

            $query->bindValue(':idpel', $data['idpel']);
            $query->bindValue(':request', $data['request']);
            $query->bindValue(':response', $data['response']);
            $query->bindValue(':request_date', $data['request_date']);
            $query->bindValue(':response_date', $data['response_date']);
            $query->bindValue(':status', $data['status']);
            $query->bindValue(':product_id', $data['product_id']);
            $query->execute();

            $last_id = $db->lastInsertId('logs_id_seq');
            $response = "Record created successfully with id " . $last_id;

		    $this->database->closeConnection();
		} catch (PDOException $e) {
		    $response = "Oops! Something went wrong: " . $e->getMessage();
		}

        return $response;
    }

    public function countProductsample($idpel) {
        $dbh = $this->database->openConnection();
        $query = $dbh->prepare("SELECT * FROM product_samples WHERE idpel = :idpel");
        $query->bindParam(':idpel', $idpel); 
        $query->execute();
        $result = $query->fetchall(); 
        return $result;
    }
    public function sendEmail() {

        include_once 'PHPMailer/PHPMailerAutoload.php';
        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'prasetyabk55@gmail.com';                 // SMTP username
        $mail->Password = 'pr45et!YA';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        $mail->setFrom('prasetyabk55@gmail.com', 'Admin');
        $mail->addAddress('prasetyabudikusumaa@gmail.com', 'User');     // Add a recipient
        $mail->addAddress('arifyulianto1994@gmail.com');               // Name is optional
        $mail->addReplyTo('info@example.com', 'Information');
        $mail->addCC('cc@example.com');
        $mail->addBCC('bcc@example.com');

        $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Layanan sedang mengalami gangguan !!!';
        $mail->Body    = 'Mohon menunggu untuk kami menindaklanjuti permasalahan kami . <b> Terima Kasih </b>';
        $mail->AltBody = 'Layanan Customer Service @199';

        // if(!$mail->send()) {
        //     echo 'Message could not be sent.';
        //     echo 'Mailer Error: ' . $mail->ErrorInfo;
        // } else {
        //     echo 'Message has been sent <br>';
        // }
    }

}
 
?>