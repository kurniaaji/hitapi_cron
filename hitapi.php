<?php
	require_once 'koneksi.php';
	require_once 'db_operation.php';

	//cek jumlah data sample
	$dbo = new DbOperation();
	$idpel = '511020213977';
	$cek = $dbo->countProductsample($idpel);


	for ($i=0; $i<count($cek); $i++) {
		//set data send api
		$send['trx_type']			="2100";
		$send['cust_msisdn']		=$cek[$i]['idpel'];
		$send['cust_account_no']	=$cek[$i]['idpel'];
		$send['product_id']			="100";
		$send['product_nomination']	="";
		$send['periode_payment']	="";
		$send['unsold']				="";


		//data json
		$data = '{"data":{"trx":{"trx_id":"0694528371","stan":"000000251965","amount":"431865","datetime":"20190206135839","merchant_code":"6021","bank_code":"4510017","rc":"0077","terminal_id":"0000000000000048","material_number":"","subscriber_id":"511020213977","subscriber_name":"ABD HAMID                ","switcher_refno":"0SYM21SB21647DE3801650167431958C","subscriber_segmentation":"R1  ","power":"900","admin_charge":"6400","outstanding_bill":"7","bill_status":"4","blth_summary":"OKT10, NOV10, DES10, JAN11","stand_meter_summary":"00014315 - 00022784","bills":[{"bill_period":"201010","produk":"PLNPOSTPAID","due_date":"20100122","meter_read_date":"00000000","total_electricity_bill":"000000087980","incentive":"00000000000","value_added_tax":"0000000000","penalty_fee":"000000018000","previous_meter_reading1":"00014315","current_meter_reading1":"00014496","previous_meter_reading2":"00000000","current_meter_reading2":"00000000","previous_meter_reading3":"00000000","current_meter_reading3":"00000000"},{"bill_period":"201011","produk":"PLNPOSTPAID","due_date":"20100220","meter_read_date":"00000000","total_electricity_bill":"000000086900","incentive":"00000000000","value_added_tax":"0000000000","penalty_fee":"000000015000","previous_meter_reading1":"00014496","current_meter_reading1":"00014675","previous_meter_reading2":"00000000","current_meter_reading2":"00000000","previous_meter_reading3":"00000000","current_meter_reading3":"00000000"},{"bill_period":"201012","produk":"PLNPOSTPAID","due_date":"20100220","meter_read_date":"00000000","total_electricity_bill":"000000084680","incentive":"00000000000","value_added_tax":"0000000000","penalty_fee":"000000012000","previous_meter_reading1":"00014232","current_meter_reading1":"00014420","previous_meter_reading2":"00000000","current_meter_reading2":"00000000","previous_meter_reading3":"00000000","current_meter_reading3":"00000000"},{"bill_period":"201101","produk":"PLNPOSTPAID","due_date":"20100220","meter_read_date":"00000000","total_electricity_bill":"000000111905","incentive":"00000000000","value_added_tax":"0000000000","penalty_fee":"000000009000","previous_meter_reading1":"00022541","current_meter_reading1":"00022784","previous_meter_reading2":"00000000","current_meter_reading2":"00000000","previous_meter_reading3":"00000000","current_meter_reading3":"00000000"}]}}}';

		//pasrse output
		$output = json_decode($data, true);
		$input 	= json_encode($send);
		//set parameter save
		$idpel				= "12345";
		$response			= $output;
		$request_date 		= date('Y-m-d H:i:s');
		$response_date 		= date('Y-m-d H:i:s');


		//set value status rc
		$setrc = substr($output['data']['trx']['rc'],-2);

		//cek value rc
		if ($setrc == '00') {
			$rc = 'Oke';
		}elseif ($setrc == '14') {
			$rc = 'Oke';
		}elseif ($setrc == '34') {
			$rc = 'Oke';
		}elseif ($setrc == '88') {
			$rc = 'Oke';
		}else{
			$rc = 'Ganguan';
		}


		//save to table product log
		$dbo = new DbOperation();
		$saveLog['idpel'] = $output['data']['trx']['subscriber_id'];
		$saveLog['request'] = $input;
		$saveLog['response'] = $data;
		$saveLog['request_date'] = $request_date;
		$saveLog['response_date'] = $response_date;
		$saveLog['status'] = $rc;
		$saveLog['product_id'] = $cek[$i]['product_id'];
		$insert = $dbo->saveDataproductlog($saveLog);

		if ($rc == 'Ganguan') {
			$send = $dbo->sendEmail();
		}
	}
?>